use core::str::FromStr;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub enum PoliticalOrientation {
    Right,
    Left,
    Both,
}

impl FromStr for PoliticalOrientation {
    type Err = ();

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        match str {
            " De droite" | "right" => Ok(PoliticalOrientation::Right),
            " De gauche" | "left" => Ok(PoliticalOrientation::Left),
            " Les deux" | "both" => Ok(PoliticalOrientation::Both),
            _ => Err(()),
        }
    }
}

impl ToString for PoliticalOrientation {
    fn to_string(&self) -> String {
        match self {
            Self::Right => " De droite".to_owned(),
            Self::Left => " De gauche".to_owned(),
            Self::Both => " Les deux".to_owned(),
        }
    }
}

pub async fn read_results() -> HashMap<String, PoliticalOrientation> {
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path("results.csv")
        .expect("unable to create csv reader from results.csv");

    let mut records: HashMap<String, PoliticalOrientation> = HashMap::new();
    for record in reader.records() {
        let record = record.expect("unable to read record");
        records.insert(
            record[0].to_string(),
            PoliticalOrientation::from_str(&record[1])
                .expect("unable to convert string to PoliticalOrientation"),
        );
    }

    records
}
