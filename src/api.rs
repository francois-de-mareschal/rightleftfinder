use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize)]
pub struct ApiRequest {
    pub input: String,
    pub price: u8,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ApiResponse {
    pub data: String,
    pub success: u16,
}
