use rightleftfinder::run;

#[tokio::main]
async fn main() {
    run().await;
}
