use crate::{
    api::{ApiRequest, ApiResponse},
    feedback::FeedbackRequest,
    results::PoliticalOrientation,
};
use arti_client::TorClient;
use arti_hyper::ArtiHttpConnector;
use hyper::{body, Body, Method, Request};
use std::str::FromStr;
use tls_api::{runtime::AsyncWriteExt, TlsConnector, TlsConnectorBuilder};
use tokio::sync::{mpsc, oneshot};
use tor_rtcompat::PreferredRuntime;

#[derive(Debug)]
pub struct Message {
    pub statement: String,
    pub wanted: PoliticalOrientation,
    pub signal: oneshot::Sender<mpsc::Sender<Message>>,
    pub sender: mpsc::Sender<Message>,
}

pub async fn tweak_result(client: TorClient<PreferredRuntime>, mut rx: mpsc::Receiver<Message>) {
    let tor_client = client.isolated_client();

    let tls_connector = tls_api_native_tls::TlsConnector::builder()
        .expect("unable to retrieve TlsConnector builder")
        .build()
        .expect("unable to build TldConnector");
    let tor_connector = ArtiHttpConnector::new(tor_client, tls_connector);

    let client = hyper::Client::builder().build::<_, hyper::Body>(tor_connector);

    let mut time_out = tokio::time::Duration::from_secs(5);

    while let Some(message) = rx.recv().await {
        let mut user_info = String::new();
        let api_request = ApiRequest {
            input: message.statement.to_owned(),
            price: 2,
        };

        let request = Request::builder()
            .method(Method::POST)
            .uri("https://degaucheoudedroite.delemazure.fr/api.php")
            .body(Body::from(
                serde_json::to_string(&api_request).expect("unable to format api request to json"),
            ))
            .expect("unable to build statement request");

        let statement = &message.statement;
        user_info.push_str(&format!("Finding political orientation of {statement}... "));
        let response = match client.request(request).await {
            Ok(response) => response,
            Err(_) => {
                eprintln!(
                    "Unable to get political orientation of {}",
                    message.statement
                );
                message
                    .signal
                    .send(message.sender)
                    .expect("unable to send back message to main loop");
                continue;
            }
        };

        let contents = body::to_bytes(response.into_body())
            .await
            .expect("unable to get request body");

        let api_response = serde_json::from_str::<ApiResponse>(
            &String::from_utf8(contents.into())
                .expect("unable to create string from contents bytes"),
        )
        .expect("unable to deserialize response from API");

        tokio::time::sleep(time_out).await;
        if &api_response.data == "too fast" {
            time_out += tokio::time::Duration::from_secs(1);
            println!("{user_info}Too fast! Retrying later.");
            message
                .signal
                .send(message.sender)
                .expect("unable to send back message to main loop");
            continue;
        }

        match PoliticalOrientation::from_str(&api_response.data)
            .expect("unable to coerce str to political to display result")
        {
            PoliticalOrientation::Right => user_info.push_str(&format!("Right-wing! ")),
            PoliticalOrientation::Left => user_info.push_str(&format!("Left-wing! ")),
            PoliticalOrientation::Both => user_info.push_str(&format!("Both sides! ")),
        }

        let mut feedback = FeedbackRequest {
            val: statement.to_owned(),
            res: api_response.data.to_owned(),
            user: false,
        };

        match PoliticalOrientation::from_str(&api_response.data)
            .expect("unable to coerce str to political to set vote")
        {
            PoliticalOrientation::Right => match message.wanted {
                PoliticalOrientation::Right => {
                    user_info.push_str(&format!("Truth has been told; voting anyway."));
                    feedback.user = true;
                }
                _ => {
                    user_info.push_str(&format!("We disagree; voting for the truth."));
                    feedback.user = false;
                }
            },
            PoliticalOrientation::Left => match message.wanted {
                PoliticalOrientation::Left => {
                    user_info.push_str(&format!("Truth has been told; voting anyway."));
                    feedback.user = true;
                }
                _ => {
                    user_info.push_str(&format!("We disagree; voting for the truth."));
                    feedback.user = false;
                }
            },
            PoliticalOrientation::Both => match message.wanted {
                PoliticalOrientation::Both => {
                    user_info.push_str(&format!("Truth has been told; voting anyway."));
                    feedback.user = true;
                }
                _ => {
                    user_info.push_str(&format!("We disagree; voting for the truth."));
                    feedback.user = false;
                }
            },
        }

        let request = Request::builder()
            .method(Method::POST)
            .uri("https://degaucheoudedroite.delemazure.fr/feedback.php")
            .body(Body::from(
                serde_json::to_string(&feedback).expect("unable to parse feedback request"),
            ))
            .expect("unable to create request for feedback");

        let _ = match client.request(request).await {
            Ok(result) => result,
            Err(_) => {
                eprintln!("Unable to send feedback for {}", message.statement);
                message
                    .signal
                    .send(message.sender)
                    .expect("unable to send back message to main loop");
                continue;
            }
        };

        println!("{user_info}");

        message
            .signal
            .send(message.sender)
            .expect("unable to send back message to main loop");

        tokio::io::stdout()
            .flush()
            .await
            .expect("unable to flush buffers to stdout");
    }
}
