use crate::manager::{tweak_result, Message};
use arti_client::{TorClient, TorClientConfig};
use futures::stream::{FuturesUnordered, StreamExt};
use tokio::{
    sync::{mpsc, oneshot},
    task::JoinSet,
};

mod api;
mod feedback;
mod manager;
mod results;

pub async fn run() {
    let tor_config = TorClientConfig::default();
    let tor_client = TorClient::create_bootstrapped(tor_config)
        .await
        .expect("unable to create bootstraped tor client");

    let statements = results::read_results().await;
    let mut statements = statements.iter().cycle();
    let mut workers = JoinSet::new();
    let mut feedbackers = FuturesUnordered::new();

    for _ in 0..16 {
        let tor_client = tor_client.clone();

        let (tx_worker, rx_worker) = mpsc::channel::<Message>(32);
        workers.spawn(tweak_result(tor_client, rx_worker));

        let (tx_back, rx_back) = oneshot::channel::<mpsc::Sender<Message>>();
        feedbackers.push(rx_back);

        let (statement, wanted) = statements.next().expect("statements shouldn't be empty");
        let message = Message {
            statement: statement.to_owned(),
            wanted: wanted.to_owned(),
            signal: tx_back,
            sender: tx_worker.clone(),
        };
        tx_worker
            .send(message)
            .await
            .expect("unable to send message to worker after spawn");
    }

    while let Some(Ok(tx_worker)) = feedbackers.next().await {
        let (statement, wanted) = statements
            .next()
            .expect("unable to get statement and wanted orientation from cyclic iterator");
        let (tx_back, rx_back) = oneshot::channel::<mpsc::Sender<Message>>();

        let tx = tx_worker.clone();
        feedbackers.push(rx_back);

        let message = Message {
            statement: statement.to_owned(),
            wanted: wanted.to_owned(),
            signal: tx_back,
            sender: tx_worker,
        };
        tx.send(message)
            .await
            .expect("unable to send work back to worker via message");
    }

    while let Some(_) = workers.join_next().await {}
}
