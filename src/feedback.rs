use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct FeedbackRequest {
    pub val: String,
    pub res: String,
    pub user: bool,
}
